from django.urls import path, include
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.Home, name='Home'),
    path('about_me/', views.about_me, name='about_me'),
    path('contacts/', views.contacts, name='contacts'),
    path('index/', views.index, name='index'),
    path('memories/', views.memories, name='memories'),
    path('base/', views.base, name='base'),
    path('navbar/', views.navbar, name='navbar'),
]