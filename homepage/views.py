from django.shortcuts import render

# Create your views here.
def about_me(request):
    return render(request, 'about_me.html')

def Home(request):
    return render(request, 'Home.html')

def contacts(request):
    return render(request, 'contacts.html')

def index(request):
    return render(request, 'index.html')

def memories(request):
    return render(request, 'memories.html')

def base(request):
    return render(request, 'base.html')

def navbar(request):
    return render(request, 'navbar.html')

