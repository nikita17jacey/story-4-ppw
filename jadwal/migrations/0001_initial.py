# Generated by Django 3.0.3 on 2020-02-27 16:41

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Jadwal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('matkul', models.CharField(max_length=100)),
                ('dosen', models.CharField(max_length=100)),
                ('sks', models.IntegerField(max_length=100)),
                ('deskripsi', models.CharField(max_length=150)),
                ('sem', models.CharField(max_length=150)),
                ('kelas', models.CharField(help_text='Masukkan tahun semester, e.g 2019/2020', max_length=150)),
            ],
        ),
    ]
