from django.urls import path
from .views import Join, delete, detail

urlpatterns = [
    path('', Join, name = 'Schedule'),
    path('detail/<int:pk>', detail, name='detail'),
    path('<int:pk>', delete, name = 'hapus'),
]